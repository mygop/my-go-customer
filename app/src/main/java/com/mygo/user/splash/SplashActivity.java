package com.mygo.user.splash;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import com.mygo.user.BuildConfig;
import com.mygo.user.GoTaxiApplication;
import com.mygo.user.R;
import com.mygo.user.api.ServiceGenerator;
import com.mygo.user.api.service.UserService;
import com.mygo.user.home.MainActivity;
import com.mygo.user.model.User;
import com.mygo.user.model.json.menu.VersionRequestJson;
import com.mygo.user.model.json.menu.VersionResponseJson;
import com.mygo.user.signIn.SignInActivity;
import com.mygo.user.utils.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {
    //TextView VersionName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        PackageInfo pInfo;
        VersionRequestJson request = new VersionRequestJson();
       // VersionName = (TextView) findViewById(R.id.VersionName);
      //  String version = BuildConfig.VERSION_NAME;
      //  VersionName.setText("Version " + version);

        int versiterbaru = BuildConfig.VERSION_CODE;
        request.version = String.valueOf(versiterbaru);
        request.application = "0";
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            request.version = pInfo.versionCode + "";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        UserService service = ServiceGenerator.createService(UserService.class, null, null);
        service.checkVersion(request).enqueue(new Callback<VersionResponseJson>() {
            @Override
            public void onResponse(Call<VersionResponseJson> call, Response<VersionResponseJson> response) {
                if (response.isSuccessful()) {
                    if (response.body().new_version.equals("yes")) {
                        showPopupUpdate(response.body().message);
                    }else if (response.body().new_version.equals("no")) {
                        start();
                        }
                }else {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SplashActivity.this);
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();
                                    start();
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            start();
                        }
                    });
                    alertDialogBuilder.setMessage(response.body().message);
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }

            @Override
            public void onFailure(Call<VersionResponseJson> call, Throwable t) {
                t.printStackTrace();
//                Toast.makeText(SplashActivity.this, "System error: " + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.e("System error:", t.getLocalizedMessage());
//                restartActivity();
                start();
            }
        });


    }

    private MaterialDialog showPopupUpdate(String message) {
        final MaterialDialog md = new MaterialDialog.Builder(this)
                .title("New Apps Available")
                .content(message)
                .icon(new IconicsDrawable(this)
                        .icon(FontAwesome.Icon.faw_google)
                        .color(Color.RED)
                        .sizeDp(24))
                .positiveText("Update")
                .negativeText("Cancel")
                .cancelable(false)
                .positiveColor(Color.BLUE)
                .negativeColor(Color.RED)
                .show();

        View positive = md.getActionButton(DialogAction.POSITIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                Log.d("AppPackage", appPackageName);
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                finish();
            }
        });
        View negative = md.getActionButton(DialogAction.NEGATIVE);
        negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
                finish();
            }

        });
        return md;
    }


    private MaterialDialog showPopupHold(String message) {
        final MaterialDialog md = new MaterialDialog.Builder(this)
                .title("Notification")
                .content(message)
                .icon(new IconicsDrawable(this)
                        .color(Color.BLUE)
                        .sizeDp(24))
                .positiveText("Close")
                .cancelable(false)
                .positiveColor(Color.DKGRAY)
                .show();

        View positive = md.getActionButton(DialogAction.POSITIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
                Intent toMain = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(toMain);
                finish();
            }
        });
        return md;
    }


    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void start() {
        User user = GoTaxiApplication.getInstance(this).getLoginUser();
        Intent intent;

        if (user != null) {
            intent = new Intent(SplashActivity.this, MainActivity.class);

        }
         else {
            intent = new Intent(SplashActivity.this, SignInActivity.class);
              }
        startActivity(intent);




    }

}
