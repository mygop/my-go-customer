package com.mygo.user.home.submenu.home;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygo.user.utils.RecyclerTouchListener;

import com.mygo.user.GoTaxiApplication;
import com.mygo.user.R;
import com.mygo.user.adapter.MainAdapterHome;
import com.mygo.user.api.ServiceGenerator;
import com.mygo.user.api.service.UserService;
import com.mygo.user.home.submenu.TopUpActivity;
import com.mygo.user.mBox.BoxActivity;
import com.mygo.user.mFood.FoodActivity;
import com.mygo.user.mMart.MartActivity;
import com.mygo.user.mMassage.MassageActivity;
import com.mygo.user.mRideCar.RideCarActivity;
import com.mygo.user.mSend.SendActivity;
import com.mygo.user.mService.mServiceActivity;
import com.mygo.user.model.Banner;
import com.mygo.user.model.Fitur;
import com.mygo.user.model.User;
import com.mygo.user.model.json.user.GetBannerResponseJson;
import com.mygo.user.model.json.user.GetSaldoRequestJson;
import com.mygo.user.model.json.user.GetSaldoResponseJson;
import com.mygo.user.splash.SplashActivity;
import com.mygo.user.utils.ConnectivityUtils;
import com.mygo.user.utils.Log;
import com.mygo.user.utils.SnackbarController;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import io.realm.Realm;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bradhawk on 10/10/2016.
 */

public class HomeFragment extends Fragment {

    public ArrayList<Banner> banners = new ArrayList<>();

     @BindView(R.id.home_mCar)
    RelativeLayout buttonGoCar;
    /*@BindView(R.id.home_mSend)
    RelativeLayout buttonGoSend;
    @BindView(R.id.home_mBox)
    RelativeLayout buttonGoBox;
    @BindView(R.id.home_mMart)
    RelativeLayout buttonGoMart;  */

    @BindView(R.id.home_mPayBalance)
    TextView mPayBalance;
    @BindView(R.id.home_topUpButton)
    Button isisaldo;
    @BindView(R.id.recyclerView_main_food)
    RecyclerView recyclerView_explore;


   // @BindView(R.id.promo_taxi)
   // LinearLayout Promo_Taxi;
   // @BindView(R.id.promo_gofood)
  //  LinearLayout Promo_Food;

    @BindView(R.id.slide_viewPager)
    AutoScrollViewPager slideViewPager;
    @BindView(R.id.slide_viewPager_indicator)
    CircleIndicator slideIndicator;
    private SnackbarController snackbarController;
    private boolean connectionAvailable;
    private boolean isDataLoaded = false;
    private Realm realm;
    private int successfulCall;
    boolean doubleBackToExitPressedOnce = false;
    private static final int REQUEST_PERMISSION_CALL = 992;

    private String[] name = {"Go Car", "Go Send", "Go Order", "Go Freight"};

    private Integer[] image =
            {
                    R.drawable.car, R.drawable.send, R.drawable.mart,
                    R.drawable.box
            };

    public static MainAdapterHome mainAdapterHome;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SnackbarController) {
            snackbarController = (SnackbarController) context;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


      buttonGoCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setTitle("Call SOS");
                alertDialogBuilder.setMessage("Do you want to call " + "999" + "?");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_CALL);
                                    return;
                                }

                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + "999"));
                                startActivity(callIntent);
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });




        /*  buttonGoSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGoSendClick();
            }
        });
        buttonGoBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGoBoxClick();
            }
        });

         buttonGoMart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGoMartClick();
            }
        });

        */



        connectionAvailable = false;

        isisaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTopUpClick();
            }
        });

        realm = GoTaxiApplication.getInstance(getActivity()).getRealmInstance();
        getImageBanner();

     recyclerView_explore.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView_explore.setLayoutManager(layoutManager);
        recyclerView_explore.setNestedScrollingEnabled(false);
        recyclerView_explore.setFocusable(false);
        mainAdapterHome = new MainAdapterHome(getActivity(), name, image);
        recyclerView_explore.setAdapter(mainAdapterHome);
        recyclerView_explore.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView_explore, new MainAdapterHome.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == REQUEST_PERMISSION_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Call permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Call permission restricted", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void navigation(int position) {

        switch (position) {

            case 0:
                Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 2).findFirst();
                System.out.println("Select..."+selectedFitur);
                Intent intent = new Intent(getActivity(), RideCarActivity.class);
                intent.putExtra(RideCarActivity.FITUR_KEY, selectedFitur.getIdFitur());
                getActivity().startActivity(intent);
                break;
            case 1:
                Fitur selectedFitursend = realm.where(Fitur.class).equalTo("idFitur", 5).findFirst();
                Intent intentsend = new Intent(getActivity(), SendActivity.class);
                intentsend.putExtra(SendActivity.FITUR_KEY, selectedFitursend.getIdFitur());
                getActivity().startActivity(intentsend);
                break;
            case 2:
                Fitur selectedFiturmart = realm.where(Fitur.class).equalTo("idFitur", 4).findFirst();
                Intent intentmart = new Intent(getActivity(), MartActivity.class);
                intentmart.putExtra(MartActivity.FITUR_KEY, selectedFiturmart.getIdFitur());
                getActivity().startActivity(intentmart);
                break;
            case 3:
                Fitur selectedFiturbox = realm.where(Fitur.class).equalTo("idFitur", 7).findFirst();
                Intent intentbox = new Intent(getActivity(), BoxActivity.class);
                intentbox.putExtra(BoxActivity.FITUR_KEY, selectedFiturbox.getIdFitur());
                getActivity().startActivity(intentbox);
                break;

            case 4:
                commingsoon();
                break;
        }
    }


    private void getImageBanner() {
        User loginUser = new User();
        if (GoTaxiApplication.getInstance(getActivity()).getLoginUser() != null) {
            loginUser = GoTaxiApplication.getInstance(getActivity()).getLoginUser();
        } else {
            startActivity(new Intent(getActivity(), SplashActivity.class));
            getActivity().finish();
        }

        UserService userService = ServiceGenerator.createService(UserService.class,
                loginUser.getEmail(), loginUser.getPassword());
        userService.getBanner().enqueue(new Callback<GetBannerResponseJson>() {
            @Override
            public void onResponse(Call<GetBannerResponseJson> call, Response<GetBannerResponseJson> response) {
                if (response.isSuccessful()) {
                    banners = response.body().data;
                    Log.e("Image", response.body().data.get(0).foto);
                    MyPagerAdapter pagerAdapter = new MyPagerAdapter(getChildFragmentManager(), banners);
                    slideViewPager.setAdapter(pagerAdapter);
                    slideIndicator.setViewPager(slideViewPager);
                    slideViewPager.setInterval(5000);
                    slideViewPager.startAutoScroll(5000);
                }
            }

            @Override
            public void onFailure(Call<GetBannerResponseJson> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        successfulCall = 0;
        connectionAvailable = ConnectivityUtils.isConnected(getActivity());
        if (!connectionAvailable) {
            if (snackbarController != null) snackbarController.showSnackbar(
                    R.string.text_noInternet, Snackbar.LENGTH_INDEFINITE, R.string.text_close,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            return;
                        }
                    });
        } else {
            updateMPayBalance();
        }
    }

    private void onGoSendClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 5).findFirst();
        Intent intent = new Intent(getActivity(), SendActivity.class);
        intent.putExtra(SendActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }
    private void onTopUpClick() {
        Intent intent = new Intent(getActivity(), TopUpActivity.class);
        startActivity(intent);
    }

    private void onGoRideClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 1).findFirst();
        Intent intent = new Intent(getActivity(), RideCarActivity.class);
        intent.putExtra(RideCarActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onGoCarClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 2).findFirst();
        Intent intent = new Intent(getActivity(), RideCarActivity.class);
        intent.putExtra(RideCarActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onGoMartClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 4).findFirst();
        Intent intent = new Intent(getActivity(), MartActivity.class);
        intent.putExtra(MartActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onGoBoxClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 7).findFirst();
        Intent intent = new Intent(getActivity(), BoxActivity.class);
        intent.putExtra(BoxActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onGoServiceClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 8).findFirst();
        Intent intent = new Intent(getActivity(), mServiceActivity.class);
        intent.putExtra(mServiceActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onGoMassageClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 6).findFirst();
        Intent intent = new Intent(getActivity(), MassageActivity.class);
        intent.putExtra(mServiceActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void onGoFoodClick() {
        Fitur selectedFitur = realm.where(Fitur.class).equalTo("idFitur", 3).findFirst();
        Intent intent = new Intent(getActivity(), FoodActivity.class);
        intent.putExtra(FoodActivity.FITUR_KEY, selectedFitur.getIdFitur());
        getActivity().startActivity(intent);
    }

    private void updateMPayBalance() {
        User loginUser = GoTaxiApplication.getInstance(getActivity()).getLoginUser();
        UserService userService = ServiceGenerator.createService(
                UserService.class, loginUser.getEmail(), loginUser.getPassword());

        GetSaldoRequestJson param = new GetSaldoRequestJson();
        param.setId(loginUser.getId());
        userService.getSaldo(param).enqueue(new Callback<GetSaldoResponseJson>() {
            @Override
            public void onResponse(Call<GetSaldoResponseJson> call, Response<GetSaldoResponseJson> response) {
                if (response.isSuccessful()) {
                    String formattedText = String.format(Locale.US, "MYR %s",
                            NumberFormat.getNumberInstance(Locale.US).format(response.body().getData()));
                    mPayBalance.setText(formattedText);
                    successfulCall++;

                    if (HomeFragment.this.getActivity() != null) {
                        Realm realm = GoTaxiApplication.getInstance(HomeFragment.this.getActivity()).getRealmInstance();
                        User loginUser = GoTaxiApplication.getInstance(HomeFragment.this.getActivity()).getLoginUser();
                        realm.beginTransaction();
                        loginUser.setmPaySaldo(response.body().getData());
                        realm.commitTransaction();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetSaldoResponseJson> call, Throwable t) {

            }

        });
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 5;
        public ArrayList<Banner> banners = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fragmentManager, ArrayList<Banner> banners) {
            super(fragmentManager);
            this.banners = banners;
        }

        @Override
        public int getCount() {
            return banners.size();
        }

        @Override
        public Fragment getItem(int position) {
            return SlideFragment.newInstance(banners.get(position).id, banners.get(position).foto);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }


    }



    private  void commingsoon(){
              String appPackageName = "com.gotaxi.user";
              startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }


}
