package com.mygo.user.model.json.book;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.mygo.user.model.DataPulsa;


/**
 * Created by Afif on 12/18/2016.
 */

public class GetDataPulsaResponseJson {

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("data")
    private DataPulsa dataPulsa;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataPulsa getDataPulsa() {
        return dataPulsa;
    }

    public void setData(DataPulsa dataPulsa) {
        this.dataPulsa = dataPulsa;
    }

}
