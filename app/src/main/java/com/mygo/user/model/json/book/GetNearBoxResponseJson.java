package com.mygo.user.model.json.book;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.mygo.user.model.Driver;

import java.util.ArrayList;

/**
 * Created by bradhawk on 10/17/2016.
 */

public class GetNearBoxResponseJson {

    @Expose
    @SerializedName("data")
    private ArrayList<Driver> data = new ArrayList<>();

    public ArrayList<Driver> getData() {
        return data;
    }

    public void setData(ArrayList<Driver> data) {
        this.data = data;
    }
}
